## Donate

### I now accept secure donations through PayPal.

If you find any of my work to be useful and would like to say thanks, or if you want to help pay for web hosting costs (keeping my site online currently costs around $5/month), then you can do so securely through PayPal.

This is a donation, not a payment for a service and is strictly optional.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="QHJG5LLAN69LN">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
