---
layout: home
name: "Dylan M. Taylor"
text: "My Personal Website and Blog"
hero:
  tagline: Welcome, please feel free to have a look around.
---

This website is a personal project that I manage as a hobby, built in order to learn and explore technology. Everything on dylanmtaylor.com is running on Linux virtual machines in the cloud. I deploy the site using [infrastructure as code](<https://gitlab.com/dylanmtaylor/terraform-dylanmtaylor-com>), and the content is periodically updated from the Git repository. All the files are served statically, and are dynamically generated  at commit time using [a CI/CD pipeline](<https://gitlab.com/dylanmtaylor/dylanmtaylor.gitlab.io/pipelines>). 

There is a small blog that I publish here, which is written in Markdown. I like to write about projects I work on as well as share some tips and tricks, and discuss relevant news. The entire source code for this site is open source and can be studied and used for free.