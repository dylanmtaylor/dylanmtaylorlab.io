---
date: 2012-03-13
title: Ordered a Raspberry Pi
description: Thoughts on purchasing the newly released Raspberry Pi Model B and plans for using this affordable computing platform.
---
## Ordered a Raspberry Pi

If you haven't heard of it, [Raspberry Pi](http://www.raspberrypi.org) is a very interesting project to create an incredibly cheap but powerful computer. It was released on February 29th, and on that same day, I purchased a Model B ($35) through element14's site. Due to overwhelming demand, I probably won't actually get it for a while, but when I do, I'll post an update on my blog about it. Because of the unbelievable value of the device, and not-too-shabby specifications, there are many potential applications for it. I plan on playing with it and potentially coding some really neat stuff in my free time. I'll post further updates about the Raspberry Pi on my blog.
