---
date: 2014-06-27
title: Speed Up Your Site With Instant Click
description: Using InstantClick technology to drastically reduce perceived loading time by preloading content when users hover over links.
---
## Speed Up Your Site With Instant Click

There are many different tools and technologies to optimize your site and make it load faster, from CloudFlare to W3 Super Cache, they all work on caching your content so the site can return it faster. One great way to decrease the latency between clicking on a link and it loading is called [InstantClick](http://instantclick.io/), and it works great. In fact, I have it running on most of my sites now. It works by starting to load the content as soon as the user hovers over the link so that the page appears to load instantaneously. The dramatic difference in perceived loading time is astounding, and for my fellow WordPress users, there's [a great plugin out there](http://wordpress.org/plugins/instantclick/) that sets it up for you. Try this out and tell me if it made your site any faster. One thing's for sure, it's great to have nearly no loading time on my site, and I plan on keeping this enabled.
